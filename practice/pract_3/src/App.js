import ContainerFilms from './components/ContainerFilms/ContainerFilms';
import './App.css';

function App() {
  return (
    <div>
      <ContainerFilms/>
    </div>
  );
}

export default App;
