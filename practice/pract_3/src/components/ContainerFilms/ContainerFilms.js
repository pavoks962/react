import React, { useState } from 'react';
import { useEffect } from 'react';

const ContainerFilms = () => {

    const [listFilm, setListFilm] = useState([]);
    const [isLoading, setLoading] = useState(true);

    const getListFilm = async () => {
        try {
            const data = await fetch('https://ajax.test-danit.com/api/swapi/films').then(res => res.json());

            setListFilm(data);
            setLoading(false)

        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }

    useEffect(() => {
        getListFilm()
    }, [])

    console.log(listFilm)

    const handleClick = (index) => {
        const newFilms = [...listFilm];
        newFilms[index].isShow = true;
        setListFilm(newFilms);
    }

    const films = listFilm.map((film, index) =>
        <div key={index}>
            < div className='wrapperItemList'>
                <li > Title: {film.name}</li>
                {!film.isShow && <button onClick={() => handleClick(index)}>More...</button>}
            </div >
            {film.isShow && (<> <p> Episode: {film.episodeId} </p>
                <p> Opening crawl: {film.openingCrawl}</p> </>)}
        </div>
    )

    return <section>
        {isLoading ? (<p>Loading...</p>) :
            <ol>
                {films}
            </ol>}
    </section>

}

export default ContainerFilms;