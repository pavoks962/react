import React from "react";
import Button from "../Button/Button";

const Numbers = ({ numbers, onDel }) => {

    // const delNumber = (numberToDel) => {
    //     const numbersUpdate = numbers.filter((number) => number !== numberToDel )
    //     return numbersUpdate
    //   }



    return <>
        <div className="numbersWrapper">
            {numbers.map((number) => (
                <div key={number}>
                    {number}
                    <Button onClick={() => onDel(number)} children='X' />
                </div>
            ))}

        </div>

    </>

}

export default Numbers;