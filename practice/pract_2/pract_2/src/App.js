
import { useEffect, useState } from 'react';
import './App.css';
import Button from './components/Button/Button';
import Numbers from './components/Numbers/Numbers';

function App() {

const [numbers, setNumbers] = useState([]);
const [isActiveButton, setIsActiveButton] = useState(false);

useEffect (() => {
  if (numbers.length === 36) {
    setIsActiveButton(true);
  } else {
    setIsActiveButton(false);
  }
}, [numbers])


const getNumbers = () => {

  if (numbers.length === 36) {
    return; 
  }

  let newNumbers;

  do {newNumbers = Math.floor(Math.random()*36) +1}
  while (numbers.includes(newNumbers)) 
  setNumbers([...numbers, newNumbers])
}

const delNumber = (numberToDel) => {
  const numbersUpdate = numbers.filter((number) => number !== numberToDel )
  return setNumbers(numbersUpdate)
}

  return (

    <div>
<Button onClick={getNumbers} disabled={numbers.length === 36} children='Generate'/>
<Numbers numbers={numbers} onDel={delNumber} />
    </div>
  );
}

export default App;
