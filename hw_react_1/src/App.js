import './App.scss';
import { useState } from 'react';
import Button from './Components/Button/Button';
import Modal from './Components/Modal/Modal';
import styles from './Components/Modal/Modal.module.scss';
import classNames from 'classnames';




function App() {

  const [modalFirstOpen, setModalFirstOpen] = useState(false);
  const [modalSecondOpen, setModalSecondOpen] = useState(false);

  const openModalFirst = () => {
    setModalFirstOpen(true)
  }

  const closeModalFirst = () => {
    setModalFirstOpen(false);
  };
  
  const openModalSecond = () => {
    setModalSecondOpen(true)
  }

  const closeModalSecond = () => {
    setModalSecondOpen(false);
  };

  return (
    <div>
      <Button backgroundColor="#077853" onClick={openModalFirst}>Open first modal</Button>
      <Button backgroundColor="#0ebee1" onClick={openModalSecond}>Open second modal</Button>
      {modalFirstOpen && (<Modal 
          modalIsOpen={modalFirstOpen} 
          setModalIsOpen={setModalFirstOpen} 
          closeModal={closeModalFirst}
          closeBtn={true}
          title="Do you want to delete this file?"
          text="Once you delete this file, it won't be possible to undo this action.
                Fre you sure you want to delete it?"
          actions= { <>
          <Button backgroundColor="#b43725">Ok</Button>
          <Button backgroundColor="#b43725" onClick={closeModalFirst}>Cancel</Button>

            {/* <button className={styles.btn} >Ok</button>
            <button className={styles.btn} onClick={closeModalFirst}>Cancel</button> */}
          </>}
          />)}
      {modalSecondOpen && (<Modal 
          modalIsOpen={modalSecondOpen} 
          setModalIsOpen={setModalSecondOpen} 
          closeModal={closeModalSecond}
          closeBtn={true}
          title="Add product to cart?"
          text="The product will be added to the cart of selected products."
          actions= { <>
            {/* <Button backgroundColor="green">Add</Button>
            <Button backgroundColor="grey" onClick={closeModalSecond}>Cancel</Button> */}
            <button className={classNames(styles.btn, styles.btnOk)} >Add</button>
            <button className={classNames(styles.btn, styles.btnCancel)} onClick={closeModalSecond}>Cancel</button>
          </>}
          />)}
    </div>
  );
}

export default App;
