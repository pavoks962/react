import React from 'react';
import styles from './Button.module.scss';

function Button({children, backgroundColor, onClick}) {

    const buttonStyle =  {
      backgroundColor: backgroundColor,
    }
      
    return <>
    <button className={styles.button} style={buttonStyle} onClick={onClick}>{children}</button> 
    </>
}
  export default Button;