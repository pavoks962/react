import React from "react";
import { useState } from "react";
import styles from './Modal.module.scss';


function Modal ({ closeBtn, actions, text, title, closeModal }) {

  const [ modalIsOpen, setModalIsOpen ] = useState(true);


    return <>

    {modalIsOpen && <div className={styles.modalWrapper} >
      <div className={styles.modalBackground} onClick={closeModal}>
      </div>
        <div className={styles.modalContainer} onClick={(e) => e.stopPropagation()}>
          <div className={styles.modalHeader}>
            <h1 className={styles.modalTitle}>{title}</h1>
            {closeBtn && (<span onClick={closeModal}>X</span>)}
          </div>
          <p className={styles.modalBody}>{text}</p>
          {actions && <div className={styles.modalFooter}>{actions}
          </div>} 
      </div>
    </div>
}
    </>
        
}

export default Modal;